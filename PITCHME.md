# Software freedom

By Vitaliy Mysak

---

@size[0.8em](Computer is a universal machine)  
@size[0.8em](But all it knows is how to get out an instruction and do it)  
@size[0.8em](Every program is a list of such instructions)  
@size[0.8em](So computer does anything you tell it to do)  

@box[bg-green text-white box-padding](Who controls your computer?)

---

@box[bg-green text-white box-padding](Closed software makes an impression that you are under control)

@box[bg-blue text-white box-padding](But reality is that <br> program obeys the instructions of the one who created it. <br> It listens to you only as much as the author will let it to)

---

@box[bg-blue text-white box-padding](What is the issue?)

Any private information can be accessed without asking your permission  
<br>
Edward Snowden showed how NSA spies on citizens  
Every phone call, email sent, location visited is being recorded  

---

## Why your privacy is in danger
- Advertisement
- Control (ex. by government)
- Bugs

---

## Consequences of privacy being broken

- No freedom of speach
- No respect for individual
- Centralization of power
- No freedom of social and political activities
- No ability to trust companies

---

## Why copyright is bad

<!-- Closed programs are bad not only because of privacy issues -->

- Information is not a resource <!-- Person who created it does not have less -->
- Freedom of knowledge <!-- Right to education as human right -->
- Economically, it is unclear that copyright has good impact \*
- Historically, knowledge is property of community, not single person

  @size[0.4em](\* https://papers.ssrn.com/sol3/papers.cfm?abstract_id=1474929)

---

# The solution

### @color[blue](Free software)
\* _not free beer_  

<!-- Software that respects your freedom -->

---

# What is free software

<!-- Software that provides 4 essential freedoms -->

- 0: Freedom to run the program as you wish  
- 1: Freedom to study and modify the program  
- 2: Freedom to share exact copies  
- 3: Freedom to share modified copies  

---

# Freedom restored

If program is free, you are allowed to choose whether you are being tracked or not  

Copyright does not work with free software  
Everyone is welcome to share and modify it  

---

# @color[red](Call to action)

### Get free software
- Linux vs Windows & MacOS
- Mastodon vs Facebook & Twitter

<br>

### Engage in development
- http://savannah.gnu.org/people

---

# Thank you

